#include "stdafx.h"
#include "ConsoleFunctions.h"

Object::Object(int x, int y)
{
	m_x = x;
	m_y = y;
	m_initialX = x;
	m_initialY = y;
}
void Object::setX(int x)
{
	m_x = x;
}

void Object::setY(int y)
{
	m_y = y;
}

int Object::getX()
{
	return m_x;
}

int Object::getY()
{
	return m_y;
}

int Object::getInitialX()
{
	return m_initialX;
}

int Object::getInitialY()
{
	return m_initialY;
}

void Object::setSymbol(char symbol)
{
	m_symbol = symbol;
}

char Object::getSymbol()
{
	return m_symbol;
}

void Object::resetPosition()
{
	setCursorPosition(m_x, m_y);
	std::cout << GROUND;
	map[m_y][m_x] = GROUND;
	m_x = m_initialX;
	m_y = m_initialY;
}

void Object::setStaticObject()
{
	map[m_y][m_x] = m_symbol;
}

void Object::drawGround()
{
	setCursorPosition(m_x, m_y);
	std::cout << GROUND;
	map[m_y][m_x] = GROUND;
}
void Object::setNewYPositionInMap(int V)
{
	m_y += V;
	map[m_y][m_x] = m_symbol;
}

void Object::setNewXPositionInMap(int H)
{
	m_x += H;
	map[m_y][m_x] = m_symbol;
}





