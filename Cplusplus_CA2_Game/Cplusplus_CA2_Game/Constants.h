#ifndef CONSTANTS_H
#define CONSTANTS_H

const int MAP_HEIGHT = 25;
const int MAP_WIDTH = 38;
//enemy movement
enum Movement {horizontal, vertical, repeatingVertical, repeatingHorizontal};

//direction on key press
enum PlayerDirection {up, down, left, right};

//UI Positions
const int SCORE_X = -4;
const int LIVES_X = 30;
const int HIGHSCORE_X = 11;
const int INSTRUCTIONS_X = 3;

//Legend Instructions
const std::string UP_KEY = "Up arrow";
const std::string DOWN_KEY = "Down arrow";
const std::string LEFT_KEY = "Left arrow";
const std::string RIGHT_KEY = "Right arrow";
const std::string BACK_SPACE = "Backspace";

//Legend Instructions
const std::string UP_DESCRIPTION = "Move Up";
const std::string LEFT_DESCRIPTION = "Move Left";
const std::string DOWN_DESCRIPTION = "Move Down";
const std::string RIGHT_DESCRIPTION = "Move Right";
const std::string BACK_SPACE_DESCRIPTION = "go back to last checkpoint";
const std::string PLAYER_DESCRIPTION = "This is you";
const std::string COIN_DESCRIPTION = "Gold coin. Worth 20 points";
const std::string CRATE_DESCRIPTION = "Movable crate. can block enemies.";
const std::string ENEMY_DESCRIPTION = "Avoid at all costs!";
const std::string CHECKPOINT_DESCRIPTION = "Keeps your progress... until death";
const std::string WIN_DESCRIPTION = "Finish. bonus score awarded!";

//Symbols
const char PLAYER_SYMBOL = '@';
const char COIN_SYMBOL = '*';
const char ENEMY_SYMBOL = '0';
const char CRATE_SYMBOL = (char)219;
const char CHECKPOINT_SYMBOL = 'C';
const char WIN_SYMBOL = 'W';
const char FILEGROUND = ' ';
const char FILEWALLS = '#';
const char WALLS = char(178);
const char GROUND = ' ';
//const char TEXTURE = ;
//for moving all elements to a different position within the console
const int XOFFSET = 10;
const int YOFFSET = 3;

//used in the main
extern bool lostLife;
extern bool hasDied;
extern bool hasWon;
#endif 