#ifndef FILEIOFUNCTIONS_H
#define FILEIOFUNCTIONS_H
#include <string>
#include <vector>
#include "Enemy.h"
#include "stdafx.h"


void readEnemiesFromFile(vector<Enemy> & enemies);
void readCoinsFromFile(vector<Coin> & coins);
void readCheckpointsFromFile(vector<Checkpoint> & checkpoints);
int readHighScoreFromFile();
void writeHighScoreToFile(int highScore);
#endif