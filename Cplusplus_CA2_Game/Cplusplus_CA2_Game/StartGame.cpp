// CplusplusCA2Game.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "ConsoleFunctions.h"
#include "FileIOFunctions.h"
bool gamerunning = true;
bool lostLife = false;
bool hasDied = false;
bool hasWon = false;

Player player = Player();
int highScore = 0;


Crate crate = Crate(24, 6);


vector <Enemy> enemies;
vector <Coin> coins;
vector<Checkpoint> checkpoints;




int main()
{
	//read all objects from file
	readObjectsFromFile(enemies, coins, checkpoints, highScore);	
	ShowConsoleCursor(false);
	convertToASCII(map);

	
	//instructions
	printInstructions();

	//map
	updateStaticObjects(coins, checkpoints);
	printMap();
	drawCoins(coins);
	
	while (gamerunning)
	{
		//update state of game - map positions
		updateState(player, crate, enemies, highScore);	

		//update what is being drawn on screen
		updateDraw(enemies, crate, player);

		//check for if player has lost life or won to reset game
		if (lostLife || hasWon)
		{		
			checkScore(player, highScore);
			resetObjects(player, crate, enemies);

			//if player fully died, objects need to be redrawn and player attributes reset
			if (hasDied || hasWon)
			{
				resetUI();
				writeHighScoreToFile(highScore);	
				player.setScore(0);
				updateStaticObjects(coins, checkpoints);
				drawCoins(coins);
				drawCheckpoints(checkpoints);
				hasDied = false;
				hasWon = false;
			}
			lostLife = false;
		}

		//using sleep as the update speed of the game
		Sleep(120);
	}

	system("pause");
}

