#include "stdafx.h"
#include "Constants.h"
#include "ConsoleFunctions.h"
Enemy::Enemy(int id, int x, int y, int direction, Movement movement)
	:Object(x,y)
{
	m_id = id;
	m_direction = direction;
	m_movement = movement;
	m_symbol = ENEMY_SYMBOL;
	m_initialDirection = direction;
}

int Enemy::getDirection() const
{
	return m_direction;
}

void Enemy::changeDirection()
{
	m_direction *= -1;
}

Movement Enemy::getMovement()
{
	return m_movement;
}
void Enemy::resetDirection()
{
	m_direction = m_initialDirection;
}
bool Enemy::Move(int V, int H, Player & player)
{
	//V is the number you want to move by in the y axis
	int y2 = m_y + V;
	//H is the number you want to move by in the x axis
	int x2 = m_x + H;

	//if the player is walking into a blank space on the x axis
	if (map[m_y][x2] == GROUND)
	{
		drawGround();
		setNewXPositionInMap(H);
		return false;
	}

	//if the player is walking into a blank space on the y axis
	if (map[y2][m_x] == GROUND)
	{
		drawGround();
		setNewYPositionInMap(V);
		return false;
	}

	if (map[y2][m_x] == PLAYER_SYMBOL || map[m_y][x2] == PLAYER_SYMBOL)
	{
		player.registerDamage();	
	}
	return true;
}


