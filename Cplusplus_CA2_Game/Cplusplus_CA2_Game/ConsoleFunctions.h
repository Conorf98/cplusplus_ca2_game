#ifndef CONSOLEFUNCTONS_H
#define CONSOLEFUNCTONS_H
#include <vector>
#include "Object.h"
#include "Player.h"
#include "Enemy.h"
#include "Crate.h"
#include "Coin.h"
#include "Constants.h"
void gameOver();
void setCursorPosition(int x, int y);
void convertToASCII(char map[MAP_HEIGHT][MAP_WIDTH]);
extern char map[MAP_HEIGHT][MAP_WIDTH];
void printMap();
void readObjectsFromFile(vector <Enemy> &enemies, vector <Coin> &coins, vector <Checkpoint> &checkpoints, int &highScore);
void Draw(Object object);
void updateDraw(vector <Enemy> enemies, Crate &crate, Player &player);
void resetObjects(Player &player, Crate &crate, vector <Enemy> &enemies);
void listenForKeys(Player &player, Crate &crate, vector <Enemy> &enemies);
void updateUI(Player &player, int &highScore);
void updateEnemyPositions(vector <Enemy> &enemies, Player &player);
void updateState(Player &player, Crate &crate, vector <Enemy> &enemies, int &highScore);
void updateStaticObjects(vector <Coin> &coins, vector <Checkpoint> &checkpoints);
void drawCoins(vector <Coin> coins);
void drawCheckpoints(vector <Checkpoint> checkpoints);
void ShowConsoleCursor(bool showFlag);
void printInstructions();
void resetUI();
void checkScore(Player &p, int &highScore);
#endif

