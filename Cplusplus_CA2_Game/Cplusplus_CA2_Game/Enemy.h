#ifndef ENEMY_H
#define ENEMY_H

#include "stdafx.h"
#pragma once
class Enemy: public Object
{
private:
	int m_id;
	int m_direction;
	int m_initialDirection;
	Movement m_movement;
public:
	Enemy(int id, int x, int y, int direction, Movement movement);
	void changeDirection();
	bool Move(int V, int H, Player &player);
	int getDirection() const;
	Movement getMovement();
	void resetDirection();
};
#endif

