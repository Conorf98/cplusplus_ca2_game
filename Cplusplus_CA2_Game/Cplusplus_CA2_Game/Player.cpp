#include "stdafx.h"
#include "ConsoleFunctions.h"
Player::Player()
	:Object(3, 3)
{
	m_checkpointX = NULL;
	m_checkpointY = NULL;
	m_lives = 3;
	m_symbol = PLAYER_SYMBOL;
}
int Player::getCheckpointX()
{
	return m_checkpointX;
}

void Player::setCheckpointX(int checkpointX)
{
	m_checkpointX = checkpointX;
}

int Player::getCheckpointY()
{
	return m_checkpointY;
}

void Player::setCheckpointY(int checkpointY)
{
	m_checkpointY = checkpointY;
}
int Player::getLives()
{
	return m_lives;
}
void Player::loseLife()
{
	if (m_lives == 1)
	{
		gameOver();		
		m_checkpointX = NULL;
		m_checkpointY = NULL;
		m_lives = 3;
		hasDied = true;
	}
	else
	{
		m_lives--;
	}
}

void Player::updateScore()
{
	m_score += 20;
}

int Player::getScore()
{
	return m_score;
}

void Player::setScore(int score)
{
	m_score = score;
}

void Player::resetPlayerPosition()
{
	drawGround();
	if (m_checkpointX == NULL && m_checkpointY == NULL)
	{
		m_x = m_initialX;
		m_y = m_initialY;
	}
	else 
	{
		m_x = m_checkpointX;
		m_y = m_checkpointY;
	}
	setCursorPosition(INSTRUCTIONS_X + MAP_WIDTH, 15);
	std::cout << "                   ";
	
}

void Player::registerDamage()
{
	setCursorPosition(INSTRUCTIONS_X + MAP_WIDTH, 15);
	std::cout << "Hit!";
	Sleep(1000);
	setCursorPosition(INSTRUCTIONS_X + MAP_WIDTH, 15);
	std::cout << "                       ";
	loseLife();
	lostLife = true;
}

void Player::playerWins()
{
	setCursorPosition(INSTRUCTIONS_X + MAP_WIDTH, 15);
	std::cout << "You Won! +2000 Score!";
	m_checkpointX = NULL;
	m_checkpointY = NULL;
	m_score += 2000;
	Sleep(2000);	
	setCursorPosition(INSTRUCTIONS_X + MAP_WIDTH, 15);
	std::cout << "                        ";
	setCursorPosition(INSTRUCTIONS_X + MAP_WIDTH, 15);
	std::cout << "Final Score: " << m_score;
	Sleep(2000);
	setCursorPosition(INSTRUCTIONS_X + MAP_WIDTH, 15);
	std::cout << "                        ";
	hasWon = true;
}


void Player::Move(int V, int H, Crate &crate, PlayerDirection direction)
{
	int crateMovement = 1;
	if (direction == PlayerDirection::up || direction == PlayerDirection::left)
	{
		crateMovement = -1;
	}
	//V is the number you want to move by in the y axis
	int y2 = m_y + V;
	//H is the number you want to move by in the x axis
	int x2 = m_x + H;

	//if the player is walking into a blank space on the x axis
	if (map[m_y][x2] == GROUND || map[m_y][x2] == COIN_SYMBOL)
	{
		if (map[m_y][x2] == COIN_SYMBOL)
		{
			updateScore();
		}
		drawGround();
		setNewXPositionInMap(H);

	}

	//if the player is walking into a blank space on the y axis
	if (map[y2][m_x] == GROUND || map[y2][m_x] == COIN_SYMBOL)
	{
		if (map[y2][m_x] == COIN_SYMBOL)
		{
			updateScore();
		}
		drawGround();

		setNewYPositionInMap(V);
	}

	if (map[m_y][x2] == CRATE_SYMBOL)
	{

		crate.Move(m_y, x2 + crateMovement);
		if (map[m_y][x2] == GROUND)
		{
			drawGround();

			setNewXPositionInMap(H);
		}
		
	}

	//if the player is walking into a crate
	if (map[y2][m_x] == CRATE_SYMBOL)
	{
		crate.Move(y2 + crateMovement, m_x);
		if (map[y2][m_x] == GROUND)
		{
			drawGround();

			setNewYPositionInMap(V);
		}
	}


	if (map[m_y][x2] == 'D')
	{
		drawGround();

		m_x = 3;
		m_y = 12;
		map[m_y][m_x] = m_symbol;

	}


	if (map[y2][m_x] == 'D')
	{
		drawGround();

		m_x = 3;
		m_y = 12;
		map[m_y][m_x] = m_symbol;
	}

	if (map[m_y][x2] == CHECKPOINT_SYMBOL)
	{
		drawGround();

		setNewXPositionInMap(H);
		m_checkpointX = m_x;
		m_checkpointY = m_y;
	}


	if (map[y2][m_x] == CHECKPOINT_SYMBOL)
	{
		drawGround();

		setNewYPositionInMap(V);
		m_checkpointX = m_x;
		m_checkpointY = m_y;
	}

	if (map[m_y][x2] == 'W')
	{
		drawGround();
		playerWins();
	}

	//if the player is walking into a blank space on the y axis
	if (map[y2][m_x] == 'W')
	{
		drawGround();
		playerWins();
	}


}






