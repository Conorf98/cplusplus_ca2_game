#ifndef CONSOLE_H
#define CONSOLE_H
#include "stdafx.h"
#include "Crate.h"
#pragma once
class Player: public Object
{
private:
	int m_lives;
	int m_checkpointX;
	int m_checkpointY;
	int m_score;
public:
	Player();
	int getCheckpointX();
	void setCheckpointX(int checkpointX);
	int getCheckpointY();
	void setCheckpointY(int checkpointY);
	int getLives();
	void updateScore();
	int getScore();
	void setScore(int score);
	void loseLife();
	void resetPlayerPosition();
	void Move(int V, int H, Crate &crate, PlayerDirection direction);
	void registerDamage();
	void playerWins();

};


#endif

