#include "stdafx.h"
#include "Checkpoint.h"


Checkpoint::Checkpoint(int id, int x, int y)
	:Object(x, y)
{
	m_id = id;	
	m_symbol = CHECKPOINT_SYMBOL;
}

int Checkpoint::getId()
{
	return m_id;
}
