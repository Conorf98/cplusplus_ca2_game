#ifndef COIN_H
#define COIN_H

#include "stdafx.h"
#pragma once
class Coin : public Object
{
private:
	int m_id;
	int m_value;
public:
	Coin(int id, int x, int y);
	int getValue();
	~Coin();
};
#endif
