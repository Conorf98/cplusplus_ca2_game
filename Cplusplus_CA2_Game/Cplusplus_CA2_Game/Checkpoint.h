#ifndef CHECKPOINT_H
#define CHECKPOINT_H
#pragma once
class Checkpoint : public Object
{
private:
	int m_id;
public:
	Checkpoint(int id, int x, int y);
	int getId();
};

#endif

