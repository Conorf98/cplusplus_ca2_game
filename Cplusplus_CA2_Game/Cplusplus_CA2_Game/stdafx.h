// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>


#include <iomanip>
#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <conio.h>
#include <cstdlib>
#include <time.h>
#include <algorithm>
#include <list>
#include <fstream>
#include "Constants.h"
#include "Windows.h"
#include "Object.h"
#include "Player.h"
#include "Enemy.h"
#include "Crate.h"
#include "Coin.h"
#include "Checkpoint.h"
using namespace std;
using std::cout;
using std::endl;
using std::cin;
using std::ofstream;
using std::ifstream;
// TODO: reference additional headers your program requires here
