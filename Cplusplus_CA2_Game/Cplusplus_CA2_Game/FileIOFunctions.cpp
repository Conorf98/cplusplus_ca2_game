#include "stdafx.h"
#include "FileIOFunctions.h"

void readEnemiesFromFile(vector<Enemy> &enemies)
{
	int xPos;
	int yPos;
	int direction;
	int movement;
	int id;
	ifstream fin("enemyData.txt");

	if (!fin)
	{
		cout << "failed to open file";
	}
	do{
		fin >> id;

		fin >> xPos;

		fin >> yPos;

		fin >> direction;

		fin >> movement;

		enemies.push_back(Enemy(id, xPos, yPos, direction, (Movement)movement));
		
	} while (!fin.eof());
	fin.close();
}

void readCoinsFromFile(vector<Coin> &coins)
{
	int id;
	int xPos;
	int yPos;
	int value;
	
	ifstream fin("coinData.txt");

	if (!fin)
	{
		cout << "failed to open file";
	}
	do {
		fin >> id;

		fin >> xPos;

		fin >> yPos;

		coins.push_back(Coin(id, xPos, yPos));

	} while (!fin.eof());
	fin.close();
}

void readCheckpointsFromFile(vector<Checkpoint>& checkpoints)
{
	int id;
	int xPos;
	int yPos;

	ifstream fin("checkpointData.txt");

	if (!fin)
	{
		cout << "failed to open file";
	}
	do {
		fin >> id;

		fin >> xPos;

		fin >> yPos;

		checkpoints.push_back(Checkpoint(id, xPos, yPos));

	} while (!fin.eof());
	fin.close();
}

int readHighScoreFromFile()
{
	int highScore;

	ifstream fin("scoreData.txt");

	if (!fin)
	{
		cout << "failed to open file";
	}
	do {
		fin >> highScore;
		
	} while (!fin.eof());
	fin.close();

	return highScore;
}

void writeHighScoreToFile(int highScore)
{
	ofstream writeHighScoreData("scoreData.txt"); //write players to file

	if (writeHighScoreData)
	{
		writeHighScoreData << highScore;
	}
	else
	{
		cout << "Error Opening file" << endl;
	}

	writeHighScoreData.close();
}

//void readMapFromFile(char map[MAP_HEIGHT][MAP_WIDTH])
//{
//	char character;
//	size_t row = 0;
//	size_t col = 0;
//	size_t   chars_read = 0;
//	ifstream fin("mapData.txt");
//	int allChars = MAP_HEIGHT * MAP_WIDTH;
//	if (!fin)
//	{
//		cout << "failed to open file";
//	}
//
//	while (fin && chars_read < allChars) {
//		fin >> map[row][col];
//		if (fin) {
//			if (++col == MAP_WIDTH) {       // Read all 10 columns in this row yet?
//				++row;                  // Move to next row
//				col = 0;                //  And don't forget to reset column!
//			}
//		}                       // Exit loop if we hit EOL (end of line)
//	}
//	fin.close();
//}
