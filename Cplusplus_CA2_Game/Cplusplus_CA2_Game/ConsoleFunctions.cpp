#include "stdafx.h"
#include "FileIOFunctions.h"
#include "ConsoleFunctions.h"
using namespace std;
using std::cout;
using std::endl;
using std::cin;

//booleans for player status
bool playerMoved = false;
bool hasCollided = false;

//generate map
char map[MAP_HEIGHT][MAP_WIDTH] =
{
	"#####################################",
	"#D    #    #   #     #     #    #   #",
	"#     #  ###   #     ##   ##      W #",
	"#  @                   # # #    #   #",
	"#     # #  ###             #   ######",
	"#     # #      #     ##   ##     #  #",
	"####### #  # # #     ### ###     # ##",
	"######## #######     #     #        #",
	"#      #   #   #     #     ######   #",
	"#      #  #     ######     ## #     #",
	"#        #   #       #     #  #     #",
	"##   ### # # #   # # #     # ##     #",
	"#    #         ###         #        #",
	"#    #    ###  ##     #    #     ####",
	"#    #######################     #  #",
	"#       #    #  #  #  #  # #        #",
	"#       #    ## # ##  #  # #     #  #",
	"#       #    #  #  #  #  #     ###  #",
	"#       #    #  #  #  #  ###     #  #",
	"#       #  #               ####  ####",
	"##      #  #               #        #",
	"##      #   ##  #  #  #  # #        #",
	"##      #    #  #  #  #  #          #",
	"##         ###############          #",
	"#####################################"
};

//used to convert the characters in the map to different characters
void convertToASCII(char map[MAP_HEIGHT][MAP_WIDTH])
{
	for (int i = 0; i < MAP_HEIGHT; i++)
	{
		for (int x = 0; x < MAP_WIDTH; x++)
		{
			if (map[i][x] == FILEWALLS)
			{
				map[i][x] = WALLS;
			}
			if (map[i][x] == FILEGROUND)
			{
				map[i][x] = GROUND;
			}
			
		}
	}
}

void readObjectsFromFile(vector <Enemy> &enemies, vector <Coin> &coins, vector <Checkpoint> &checkpoints, int &highScore)
{
	readEnemiesFromFile(enemies);
	readCoinsFromFile(coins);
	readCheckpointsFromFile(checkpoints);
	highScore = readHighScoreFromFile();
}
//triggered on death
 void gameOver()
 {
	 setCursorPosition(INSTRUCTIONS_X + MAP_WIDTH, 15);
	 std::cout << "You are dead";
	 
	 //pause for a second
	 Sleep(1000);	 
 }

 //found on stackoverflow -- used to set the console cursor position using the x and y axis
 void setCursorPosition(int x, int y)
 {
	 static const HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
	 std::cout.flush();
	 COORD coord = { (SHORT)x +XOFFSET, (SHORT)y +YOFFSET };
	 SetConsoleCursorPosition(hOut, coord);
 }

 //prints the map once
 void printMap()
 {
	 HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	 //set the map colour to green/blue
	 SetConsoleTextAttribute(hStdOut, FOREGROUND_BLUE | FOREGROUND_GREEN);
	 for (int i = 0; i < MAP_HEIGHT; i++) 
	 {
		 setCursorPosition(0, i);
		 cout << map[i] << endl;
	 }
 }

 //multiple different objects are passed into this function and will be coloured differently based on their symbol
 void Draw(Object object)
 {
	 HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	 if (object.getSymbol() == ENEMY_SYMBOL)
	 {
		 SetConsoleTextAttribute(hStdOut, FOREGROUND_RED); //enemy is red
	 }
	 else if (object.getSymbol() == COIN_SYMBOL)
	 {

		 SetConsoleTextAttribute(hStdOut, FOREGROUND_RED | FOREGROUND_GREEN); // coin is yellow
	 }
	 else
	 {
		 SetConsoleTextAttribute(hStdOut, FOREGROUND_BLUE | FOREGROUND_GREEN); //anything else is green/blue
	 }
	 setCursorPosition(object.getX(), object.getY());
	 std::cout << object.getSymbol();
 }
 
 //updates all dynamic object positions
 void updateDraw(vector <Enemy> enemies, Crate &crate, Player &player)
 {

	 for (Enemy &e : enemies)
	 {
		 Draw(e);
	 }

	 Draw(crate);

	 //player will only updated if he moves
	 if (playerMoved)
	 {
		 Draw(player);
	 }

 }

 //triggered when player dies / wins
 void resetObjects(Player &player, Crate &crate, vector <Enemy> &enemies)
 {

	 //put objects in their initial positions
	 player.resetPlayerPosition();
	 crate.resetPosition();
	 crate.setStaticObject();
	 for (Enemy &e : enemies)
	 {
		 e.resetPosition();
		 e.resetDirection();
	 }
	 //player will need to be drawn here as he will be updated but not drawn otherwise
	 Draw(player);
 }

 //method that handles key input
 void listenForKeys(Player &player, Crate &crate, vector <Enemy> &enemies)
 {
	 if (GetAsyncKeyState(VK_UP))     //player moves up
	 {
		 player.Move(-1, 0, crate, PlayerDirection::up);        //change y value
		 playerMoved = true;
	 }

	 if (GetAsyncKeyState(VK_DOWN))
	 {
		 player.Move(1, 0, crate, PlayerDirection::down);
		 playerMoved = true;
	 }

	 if (GetAsyncKeyState(VK_RIGHT)) //player moves right
	 {
		 player.Move(0, 1, crate, PlayerDirection::right);  //change x value
		 playerMoved = true;
	 }

	 if (GetAsyncKeyState(VK_LEFT))
	 {
		 player.Move(0, -1, crate, PlayerDirection::left);
		 playerMoved = true;
	 }

	 if (GetAsyncKeyState(VK_BACK)) //used to put the player back to last checkpoint
	 {
		 resetObjects(player, crate, enemies);
	 }
 }

 //update the players score and lives
 void updateUI(Player &player, int &highScore)
 {
	 setCursorPosition(SCORE_X, 1 - YOFFSET);
	 std::cout << "Score: " << player.getScore();
	 setCursorPosition(LIVES_X, 1 - YOFFSET);
	 std::cout << "Player Lives: " << player.getLives();
	 setCursorPosition(HIGHSCORE_X, 1 - YOFFSET);
	 std::cout << "HighScore: " << highScore;
 }


 //area where different enemy interactions are coded
 void updateEnemyPositions(vector <Enemy> &enemies, Player &player)
 {
	 for (Enemy &e : enemies)
	 {

		 if (e.getMovement() == vertical || e.getMovement() == repeatingVertical)
		 {
			 hasCollided = e.Move(e.getDirection(), 0, player);
		 }
		 if (e.getMovement() == horizontal || e.getMovement() == repeatingHorizontal)
		 {

			 hasCollided = e.Move(0, e.getDirection(), player);
		 }
		 if (hasCollided)
		 {
			 if (e.getMovement() == repeatingVertical || e.getMovement() == repeatingHorizontal)
			 {
				 //if enemy repeats, put him back to his initial position to loop again
				 e.resetPosition();

			 }
			 else
			 {
				 //if the enemy collides with anything, make it turn around and go back
				 e.changeDirection();
			 }
		 }
	 }
 }

 //updates objects positions within the map
 void updateState(Player &player, Crate &crate, vector <Enemy> &enemies, int &highScore)
 {
	 playerMoved = false;

	 updateUI(player,highScore);
	 updateEnemyPositions(enemies,player);
	 listenForKeys(player, crate, enemies);
 }

 //called at the beginning to place all static objects in the map
 void updateStaticObjects(vector <Coin> &coins, vector <Checkpoint> &checkpoints) //coins
 {
	 //crate is placed in its initial position
	 map[6][24] = CRATE_SYMBOL;
	 for (Coin &c : coins)
	 {
		 c.setStaticObject();
	 }

	 for (Checkpoint &c : checkpoints)
	 {
		 c.setStaticObject();
	 }
 }

 //draw all coins
 void drawCoins(vector <Coin> coins)
 {
	 for (Coin &c : coins)
	 {
		 Draw(c);
	 }
 }
 //draw all Checkpoints
 void drawCheckpoints(vector <Checkpoint> checkpoints)
 {
	 for (Checkpoint &c : checkpoints)
	 {
		 Draw(c);
	 }
 }


 //from :https://stackoverflow.com/questions/18028808/blinking-underscore-with-console
 //using it to remove the flashing white cursor on the console
 void ShowConsoleCursor(bool showFlag)
 {
	 HANDLE out = GetStdHandle(STD_OUTPUT_HANDLE);

	 CONSOLE_CURSOR_INFO     cursorInfo;

	 GetConsoleCursorInfo(out, &cursorInfo);
	 cursorInfo.bVisible = showFlag; // set the cursor visibility
	 SetConsoleCursorInfo(out, &cursorInfo);
 }

 //instructions of game printed to right of game screen
 void printInstructions()
 {
	 setCursorPosition(INSTRUCTIONS_X + MAP_WIDTH, 0);
	 std::cout << "LEGEND" << endl;
	 setCursorPosition(INSTRUCTIONS_X + MAP_WIDTH, 2);
	 std::cout << UP_KEY << " -> " << UP_DESCRIPTION << endl;
	 setCursorPosition(INSTRUCTIONS_X + MAP_WIDTH, 3);
	 std::cout << RIGHT_KEY << " -> " << RIGHT_DESCRIPTION << endl;
	 setCursorPosition(INSTRUCTIONS_X + MAP_WIDTH, 4);
	 std::cout << DOWN_KEY << " -> " << DOWN_DESCRIPTION << endl;
	 setCursorPosition(INSTRUCTIONS_X + MAP_WIDTH, 5);
	 std::cout << LEFT_KEY << " -> " << LEFT_DESCRIPTION << endl;
	 setCursorPosition(INSTRUCTIONS_X + MAP_WIDTH, 6);
	 std::cout << BACK_SPACE << " -> " << BACK_SPACE_DESCRIPTION << endl;
	 setCursorPosition(INSTRUCTIONS_X + MAP_WIDTH, 8);
	 std::cout << PLAYER_SYMBOL << " -> " << PLAYER_DESCRIPTION << endl;
	 setCursorPosition(INSTRUCTIONS_X + MAP_WIDTH, 9);
	 std::cout << CRATE_SYMBOL << " -> " << CRATE_DESCRIPTION << endl;
	 setCursorPosition(INSTRUCTIONS_X + MAP_WIDTH, 10);
	 std::cout << COIN_SYMBOL << " -> " << COIN_DESCRIPTION << endl;
	 setCursorPosition(INSTRUCTIONS_X + MAP_WIDTH, 11);
	 std::cout << ENEMY_SYMBOL << " -> " << ENEMY_DESCRIPTION << endl;
	 setCursorPosition(INSTRUCTIONS_X + MAP_WIDTH, 12);
	 std::cout << CHECKPOINT_SYMBOL << " -> " << CHECKPOINT_DESCRIPTION << endl;
	 setCursorPosition(INSTRUCTIONS_X + MAP_WIDTH, 13);
	 std::cout << WIN_SYMBOL << " -> " << WIN_DESCRIPTION << endl;
 }

 //reset the score display on death/ win
 void resetUI()
 {
	 setCursorPosition(SCORE_X, 1 - YOFFSET);
	 std::cout << "Score:     ";
 }

 //change the highscore to current score if it is greater
 void checkScore(Player &p, int &highScore)
 {
	 if (p.getScore() > highScore)
	 {
		 highScore = p.getScore();
	 }
 }

 

