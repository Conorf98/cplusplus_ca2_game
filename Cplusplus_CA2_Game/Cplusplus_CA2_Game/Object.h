#ifndef POSITION_H
#define POSITION_H


#pragma once
class Object  //ADD SYMBOL TO VARIABLES
{
protected:
	int m_x;
	int m_y;
	int m_initialX;
	int m_initialY;
	char m_symbol;
public:
	Object(int x, int y);
	void setX(int x);
	void setY(int y);	
	int getX();
	int getY();
	int getInitialX();
	int getInitialY();
	void setSymbol(char symbol);
	char getSymbol();
	void resetPosition();
	void setStaticObject();
	void drawGround();
	void setNewYPositionInMap(int V);
	void setNewXPositionInMap(int H);
};
#endif // !POSITION_H

