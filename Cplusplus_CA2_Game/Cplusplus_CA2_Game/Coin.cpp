#include "stdafx.h"
#include "Coin.h"

Coin::Coin(int id, int x, int y)
	:Object(x,y)
{
	m_id = id;
	m_value = 20;
	m_symbol = COIN_SYMBOL;
}

int Coin::getValue()
{
	return m_value;
}

Coin::~Coin()
{
}
